package fr.cnds.malt;

import org.junit.Test;

import fr.cnds.malt.models.Partner;
import org.junit.Assert;

public class IpServiceTests {
	@Test
	public void testIpServiceForUS() {
		Partner client = new Partner();
		client.setIp("8.8.8.8");
		Assert.assertEquals(client.getCountry(), "US");
	}
	@Test
	public void testIpServiceForES() {
		Partner client = new Partner();
		client.setIp("217.127.206.227");
		Assert.assertEquals(client.getCountry(), "ES");
	}
	@Test
	public void testInvalidIP() {
		Partner client = new Partner();
		client.setIp("5.5.5");
		Assert.assertEquals(client.getCountry(), Constants.NOT_FOUND);
	}
}
