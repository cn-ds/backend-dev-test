package fr.cnds.malt;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import fr.cnds.malt.controllers.CalculationController;
import fr.cnds.malt.controllers.RuleController;
import fr.cnds.malt.models.CalculateFeeRequest;
import fr.cnds.malt.models.CommercialRelation;
import fr.cnds.malt.models.Fee;
import fr.cnds.malt.models.RuleDefinition;
import fr.cnds.malt.models.entity.AbstractEntity;

public class LtBlocTests {
	RuleController ruleController = new RuleController();

	@After
	public void clearRules() {
		RuleController.reinitRules();
	}
	
	@Before
	public void addSomeRules() {
		/**
		 * Creating rule 2 checking commercialRelation is under 2 months
		 */
		AbstractEntity blocLt = new AbstractEntity();
		blocLt.setOp(Constants.LT);
		blocLt.setField("commercialRelation.firstMission.duration");
		blocLt.setValue("2months");
		RuleDefinition rule2 = new RuleDefinition();
		rule2.setFee(new Fee(2, "test2"));
		rule2.setId("test2");
		rule2.setName("test2");
		List<AbstractEntity> chain2 = new ArrayList<>();
		chain2.add(blocLt);
		rule2.setrule(chain2);
		ruleController.createRule(rule2);
	}
	
	
	@Test
	public void testLtBloc() {
		CalculateFeeRequest feeRequest = new CalculateFeeRequest();
		CommercialRelation relation = new CommercialRelation();
		relation.setFirstMission("2018-04-16 13:24:17.510Z");
		feeRequest.setCommercialRelation(relation);
		CalculationController calculationController = new CalculationController();
		Fee fee = calculationController.getFeeToApply(feeRequest);
		Assert.assertEquals(10, fee.getRate());
	}
}
