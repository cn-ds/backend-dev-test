package fr.cnds.malt;

import java.util.List;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import fr.cnds.malt.controllers.CalculationController;
import fr.cnds.malt.controllers.RuleController;
import fr.cnds.malt.models.CalculateFeeRequest;
import fr.cnds.malt.models.Fee;
import fr.cnds.malt.models.Partner;
import fr.cnds.malt.models.RuleDefinition;
import fr.cnds.malt.models.entity.AbstractEntity;

import org.junit.After;
import org.junit.Assert;

public class BlocTests {

	RuleController ruleController = new RuleController();

	@After
	public void clearRules() {
		RuleController.reinitRules();
	}
	
	@Before
	public void addSomeRules() {
		/**
		 * Creating rule 1 checking only client country is ES
		 */
		AbstractEntity blocEq = new AbstractEntity();
		blocEq.setOp(Constants.EQ);
		blocEq.setField("client.country");
		blocEq.setValue("ES");
		RuleDefinition rule1 = new RuleDefinition();
		rule1.setFee(new Fee(1, "test1"));
		rule1.setId("test1");
		rule1.setName("test1");
		List<AbstractEntity> chain1 = new ArrayList<>();
		chain1.add(blocEq);
		rule1.setrule(chain1);
		ruleController.createRule(rule1);
		/**
		 * Creating rule 2 checking that:
		 * 		client country is US
		 * 		freelancer country is US
		 */
		AbstractEntity blocEq1 = new AbstractEntity();
		blocEq1.setOp(Constants.EQ);
		blocEq1.setField("client.country");
		blocEq1.setValue("US");
		AbstractEntity blocEq2 = new AbstractEntity();
		blocEq2.setOp(Constants.EQ);
		blocEq2.setField("freelancer.country");
		blocEq2.setValue("US");
		RuleDefinition rule2 = new RuleDefinition();
		rule2.setFee(new Fee(2, "test2"));
		rule2.setId("test2");
		rule2.setName("test2");
		List<AbstractEntity> chain2 = new ArrayList<>();
		chain2.add(blocEq1);
		chain2.add(blocEq2);
		rule2.setrule(chain2);
		ruleController.createRule(rule2);
		/**
		 * Creating rule 2 checking that:
		 * 		client city is "Madrid" 
		 * 			OR
		 * 		freelancer country is "Lyon"
		 */
		AbstractEntity blocEq3 = new AbstractEntity();
		blocEq3.setOp(Constants.EQ);
		blocEq3.setField("client.city");
		blocEq3.setValue("Madrid");
		AbstractEntity blocEq4 = new AbstractEntity();
		blocEq4.setOp(Constants.EQ);
		blocEq4.setField("freelancer.city");
		blocEq4.setValue("Lyon");
		RuleDefinition rule3 = new RuleDefinition();
		rule3.setFee(new Fee(3, "test3"));
		rule3.setId("test3");
		rule3.setName("test3");
		List<AbstractEntity> chain3 = new ArrayList<>();
		chain3.add(blocEq3);
		chain3.add(blocEq4);
		AbstractEntity blocOr1 = new AbstractEntity();
		blocOr1.setOp(Constants.OR);
		blocOr1.setChain(chain3);
		List<AbstractEntity> chain4 = new ArrayList<>();
		chain4.add(blocOr1);
		rule3.setrule(chain4);
		ruleController.createRule(rule3);
	}
	
	@Test
	public void testNoRulesMeet() {
		CalculateFeeRequest feeRequest = new CalculateFeeRequest();
		Partner client = new Partner();
		client.setCountry("FR");
		Partner freelancer = new Partner();
		freelancer.setCountry("US");
		feeRequest.setClient(client);
		feeRequest.setFreelancer(freelancer);
		CalculationController calculationController = new CalculationController();
		Fee fee = calculationController.getFeeToApply(feeRequest);
		Assert.assertEquals(10, fee.getRate());
	}
	
	@Test
	public void testAndChainAndEqualsBloc() {
		CalculateFeeRequest feeRequest = new CalculateFeeRequest();
		Partner client = new Partner();
		client.setCountry("US");
		Partner freelancer = new Partner();
		freelancer.setCountry("US");
		feeRequest.setClient(client);
		feeRequest.setFreelancer(freelancer);
		CalculationController calculationController = new CalculationController();
		Fee fee = calculationController.getFeeToApply(feeRequest);
		Assert.assertEquals(2, fee.getRate());
	}
	
	@Test
	public void testEqBloc() {
		CalculateFeeRequest feeRequest = new CalculateFeeRequest();
		Partner client = new Partner();
		client.setIp("217.127.206.227");
		Partner freelancer = new Partner();
		freelancer.setIp("217.127.206.227");
		feeRequest.setClient(client);
		feeRequest.setFreelancer(freelancer);
		CalculationController calculationController = new CalculationController();
		Fee fee = calculationController.getFeeToApply(feeRequest);
		Assert.assertEquals(1, fee.getRate());
	}
	
	@Test
	public void testOrChain() {
		CalculateFeeRequest feeRequest = new CalculateFeeRequest();
		Partner client = new Partner();
		client.setCity("Paris");
		Partner freelancer = new Partner();
		freelancer.setCity("Lyon");
		feeRequest.setClient(client);
		feeRequest.setFreelancer(freelancer);
		CalculationController calculationController = new CalculationController();
		Fee fee = calculationController.getFeeToApply(feeRequest);
		Assert.assertEquals(3, fee.getRate());
	}
	
	@Test
	/**
	 * This tests meets 2 rules the rule 1 and the rule 3
	 * As the rule 1 as the lowest fee it is the one to be applied
	 */
	public void testSelectTheLowestFeeToApplyChain() {
		CalculateFeeRequest feeRequest = new CalculateFeeRequest();
		Partner client = new Partner();
		client.setCountry("ES");
		client.setCity("Madrid");
		Partner freelancer = new Partner();
		freelancer.setCity("Lyon");
		feeRequest.setClient(client);
		feeRequest.setFreelancer(freelancer);
		CalculationController calculationController = new CalculationController();
		Fee fee = calculationController.getFeeToApply(feeRequest);
		Assert.assertEquals(1, fee.getRate());
	}
	
}
