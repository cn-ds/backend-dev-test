package fr.cnds.malt;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import fr.cnds.malt.controllers.CalculationController;
import fr.cnds.malt.controllers.RuleController;
import fr.cnds.malt.models.CalculateFeeRequest;
import fr.cnds.malt.models.CommercialRelation;
import fr.cnds.malt.models.Fee;
import fr.cnds.malt.models.RuleDefinition;
import fr.cnds.malt.models.entity.AbstractEntity;

public class NoopTests {
	RuleController ruleController = new RuleController();

	@After
	public void clearRules() {
		RuleController.reinitRules();
	}
	
	@Before
	public void addSomeRules() {
		/**
		 * Creating rule 1 checking commercialRelation is over 2 months
		 */
		AbstractEntity blocGt = new AbstractEntity();
		blocGt.setOp("BlocThatDoesNotExist");
		blocGt.setField("commercialRelation.firstMission.duration");
		blocGt.setValue("2months");
		RuleDefinition rule1 = new RuleDefinition();
		rule1.setFee(new Fee(1, "test1"));
		rule1.setId("test1");
		rule1.setName("test1");
		List<AbstractEntity> chain1 = new ArrayList<>();
		chain1.add(blocGt);
		rule1.setrule(chain1);
		ruleController.createRule(rule1);
	}
	
	@Test
	public void testNoopBloc() {
		CalculateFeeRequest feeRequest = new CalculateFeeRequest();
		CommercialRelation relation = new CommercialRelation();
		relation.setFirstMission("2018-04-16 13:24:17.510Z");
		feeRequest.setCommercialRelation(relation);
		CalculationController calculationController = new CalculationController();
		Fee fee = calculationController.getFeeToApply(feeRequest);
		Assert.assertEquals(10, fee.getRate());
	}
}
