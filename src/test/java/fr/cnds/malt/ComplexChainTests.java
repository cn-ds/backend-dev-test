package fr.cnds.malt;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import fr.cnds.malt.controllers.CalculationController;
import fr.cnds.malt.controllers.RuleController;
import fr.cnds.malt.models.CalculateFeeRequest;
import fr.cnds.malt.models.CommercialRelation;
import fr.cnds.malt.models.Fee;
import fr.cnds.malt.models.Mission;
import fr.cnds.malt.models.Partner;
import fr.cnds.malt.models.RuleDefinition;
import fr.cnds.malt.models.entity.AbstractEntity;

public class ComplexChainTests {
	RuleController ruleController = new RuleController();

	@After
	public void clearRules() {
		RuleController.reinitRules();
	}
	
	@Before
	public void addSomeRules() {
		/**
		 * Creating rule 1 checking commercialRelation is over 2 months
		 */
		AbstractEntity blocGt1 = new AbstractEntity();
		blocGt1.setOp(Constants.GT);
		blocGt1.setField("commercialRelation.lastMission.duration");
		blocGt1.setValue("2months");
		AbstractEntity blocGt2 = new AbstractEntity();
		blocGt2.setOp(Constants.GT);
		blocGt2.setField("mission.lenght");
		blocGt2.setValue("2months");
		AbstractEntity blocEq1 = new AbstractEntity();
		blocEq1.setOp(Constants.EQ);
		blocEq1.setField("client.country");
		blocEq1.setValue("ES");
		AbstractEntity blocEq2 = new AbstractEntity();
		blocEq2.setOp(Constants.EQ);
		blocEq2.setField("freelancer.country");
		blocEq2.setValue("ES");
		RuleDefinition rule1 = new RuleDefinition();
		rule1.setFee(new Fee(8, "Spain or repeat"));
		rule1.setId("Spain or repeat");
		rule1.setName("Spain or repeat");
		List<AbstractEntity> chain1 = new ArrayList<>();
		chain1.add(blocGt1);
		chain1.add(blocGt2);
		AbstractEntity blocOr = new AbstractEntity();
		blocOr.setOp(Constants.OR);
		blocOr.setChain(chain1);
		List<AbstractEntity> chain2 = new ArrayList<>();
		chain2.add(blocOr);
		chain2.add(blocEq1);
		chain2.add(blocEq2);
		rule1.setrule(chain2);
		ruleController.createRule(rule1);
	}
	
	@Test
	public void testComplexChain() {
		Partner client = new Partner();
		client.setIp("217.127.206.227");
		Partner freelancer = new Partner();
		freelancer.setIp("217.127.206.227");
		Mission mission = new Mission();
		mission.setLenght("4months");
		CommercialRelation relation = new CommercialRelation();
		relation.setFirstMission("2018-04-16 13:24:17.510Z");
		relation.setLastMission("2018-07-16 14:24:17.510Z");
		CalculateFeeRequest feeRequest = new CalculateFeeRequest();
		feeRequest.setClient(client);
		feeRequest.setFreelancer(freelancer);
		feeRequest.setMission(mission);
		feeRequest.setCommercialRelation(relation);
		CalculationController calculationController = new CalculationController();
		Fee fee = calculationController.getFeeToApply(feeRequest);
		Assert.assertEquals(8, fee.getRate());
	}
}
