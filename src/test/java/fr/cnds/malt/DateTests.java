package fr.cnds.malt;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Test;

import fr.cnds.malt.controllers.CalculationController;
import fr.cnds.malt.controllers.RuleController;
import fr.cnds.malt.models.CalculateFeeRequest;
import fr.cnds.malt.models.Fee;
import fr.cnds.malt.models.Mission;
import fr.cnds.malt.models.RuleDefinition;
import fr.cnds.malt.models.entity.AbstractEntity;
import fr.cnds.malt.utils.DateUtils;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;

public class DateTests {

	RuleController ruleController = new RuleController();

	@After
	public void clearRules() {
		RuleController.reinitRules();
	}
	
	@Before
	public void addSomeRules() {
		/**
		 * Creating rule 1 checking mission duration is equal to 2 months
		 */
		AbstractEntity blocEq = new AbstractEntity();
		blocEq.setOp(Constants.EQ);
		blocEq.setField("mission.lenght");
		blocEq.setValue("2months");
		RuleDefinition rule1 = new RuleDefinition();
		rule1.setFee(new Fee(1, "test1"));
		rule1.setId("test1");
		rule1.setName("test1");
		List<AbstractEntity> chain1 = new ArrayList<>();
		chain1.add(blocEq);
		rule1.setrule(chain1);
		ruleController.createRule(rule1);
		/**
		 * Creating rule 2 checking mission duration is equal to 1 year
		 */
		AbstractEntity blocEq2 = new AbstractEntity();
		blocEq2.setOp(Constants.EQ);
		blocEq2.setField("mission.lenght");
		blocEq2.setValue("1year");
		RuleDefinition rule2 = new RuleDefinition();
		rule2.setFee(new Fee(2, "test2"));
		rule2.setId("test2");
		rule2.setName("test2");
		List<AbstractEntity> chain2 = new ArrayList<>();
		chain2.add(blocEq2);
		rule2.setrule(chain2);
		ruleController.createRule(rule2);
	}
	
	@Test
	public void testDateUtils() {
		String date = "2018-04-16 13:24:17.510Z";
		Date parsedDate = DateUtils.parseDate(date);
		GregorianCalendar tmpDate = new GregorianCalendar(2018, 3/* month + 1 */, 16, 13, 24, 17);
		tmpDate.set(Calendar.MILLISECOND, 510);
		Date anotherDate = tmpDate.getTime();
		Assert.assertEquals(0, parsedDate.compareTo(anotherDate));
	}
	
	@Test
	public void testDateUtilsParseFail() {
		String date = "2018-dfgdfgdfg04-16 13:24:17.510Z";
		Date parsedDate = DateUtils.parseDate(date);
		Date anotherDate = new Date();
		Assert.assertEquals(0, parsedDate.compareTo(anotherDate));
	}
	
	@Test
	public void testDateEqBloc() {
		CalculateFeeRequest feeRequest = new CalculateFeeRequest();
		Mission mission = new Mission();
		mission.setLenght("2months");
		feeRequest.setMission(mission);
		CalculationController calculationController = new CalculationController();
		Fee fee = calculationController.getFeeToApply(feeRequest);
		Assert.assertEquals(1, fee.getRate());
	}
	
	@Test
	public void testDateEqBloc2() {
		CalculateFeeRequest feeRequest = new CalculateFeeRequest();
		Mission mission = new Mission();
		mission.setLenght("1year");
		feeRequest.setMission(mission);
		CalculationController calculationController = new CalculationController();
		Fee fee = calculationController.getFeeToApply(feeRequest);
		Assert.assertEquals(2, fee.getRate());
	}
}
