package fr.cnds.malt;

public class Messages {
	public static String INCORRECT_RATE = "A rule led to a rate lower than 0 to be applied. It was canceled";
	public static String PARSE_DATE_ERROR = "Couldn't parse the date :";
}
