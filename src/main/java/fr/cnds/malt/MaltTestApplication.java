package fr.cnds.malt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MaltTestApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(MaltTestApplication.class, args);
	}

}

