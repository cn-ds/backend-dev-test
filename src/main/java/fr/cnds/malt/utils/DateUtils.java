package fr.cnds.malt.utils;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.cnds.malt.Constants;
import fr.cnds.malt.Messages;

public class DateUtils {
	private static final Logger logger = LoggerFactory.getLogger(DateUtils.class);

	public static Date parseDate(String date) {
		try {
			return Constants.FORMATER.parse(date);
		} catch (ParseException e) {
			logger.error(Messages.PARSE_DATE_ERROR + e);
			return new Date();
		}
	}
	
	public static Map<String, Integer> timeScaleConversionRate;
	static {
		timeScaleConversionRate = new HashMap<String, Integer>();
		timeScaleConversionRate.put(Constants.DAYS, 1);
		timeScaleConversionRate.put(Constants.MONTHS, 30);
		timeScaleConversionRate.put(Constants.YEARS, 365);
	}
}
