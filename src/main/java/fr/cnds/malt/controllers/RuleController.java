package fr.cnds.malt.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.cnds.malt.models.RuleDefinition;

@RestController
@EnableAutoConfiguration
public class RuleController {
	public static List<RuleDefinition> allTheRules = new ArrayList<>();
	
	@GetMapping("/rule")
	List<RuleDefinition> getRules() {
		return allTheRules;
	}
	
	@PostMapping("/rule")
	public void createRule(@RequestBody RuleDefinition newRule) {
		newRule.getFee().setReason(newRule.getName());
		allTheRules.add(newRule);
	}
	
	/**
	 * Method used for unique tests
	 */
	public static void reinitRules() {
		allTheRules = new ArrayList<>();
	}
}
