package fr.cnds.malt.controllers;

import java.util.Map;
import java.util.Optional;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.cnds.malt.models.CalculateFeeRequest;
import fr.cnds.malt.models.Fee;

@RestController
@EnableAutoConfiguration
public class CalculationController {

	@PostMapping("/calculateFee")
	public Fee getFeeToApply(@RequestBody CalculateFeeRequest feeRequest) {
		ObjectMapper objectMapper = new ObjectMapper();
		Map<String, Object> fieldMap = objectMapper.convertValue(feeRequest, new TypeReference<Map<String,Object>>() {});
		Optional<Fee> minFee = RuleController.allTheRules.stream()
			.filter(rule -> rule.getRule().evaluate(fieldMap))
			.map(rule -> rule.getFee())
			.min(Fee::compareTo);
		
		if (minFee.isPresent()) {
			return minFee.get();
		} else {
			return new Fee();
		}
	}
}
