package fr.cnds.malt.models;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.cnds.malt.Messages;

public class Fee implements Comparable<Fee>{
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private int rate;
	private String reason;
	
	/**
	 * The default fee is a rate of 10% and it should always be applied.
	 */
	public Fee() {
		super();
		this.rate = 10;
		this.reason = "Default fee";
	}
	
	public Fee(int rate, String reason) {
		super();
		this.rate = rate;
		this.reason = reason;
	}
	
	public int getRate() {
		return rate;
	}
	
	public void setRate(int rate) {
		if (rate > 0) {
			this.rate = rate;
		} else {
			logger.warn(Messages.INCORRECT_RATE);
			setReason(Messages.INCORRECT_RATE);
		}
	}
	
	public String getReason() {
		return reason;
	}
	
	public void setReason(String reason) {
		this.reason = reason;
	}

	@Override
	public int compareTo(Fee fee) {
		return Integer.compare(this.getRate(), fee.getRate());
	}
}
