package fr.cnds.malt.models;

import java.util.List;

import fr.cnds.malt.models.entity.AbstractEntity;
import fr.cnds.malt.models.entity.chains.AndChain;

public class RuleDefinition {
	private String id;
	private String name;
	private Fee fee;
	private List<AbstractEntity> rule;
		
	public RuleDefinition() {
		super();
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setrule(List<AbstractEntity> mainChain) {
		this.rule = mainChain;
	}
	
	public AbstractEntity getRule() {
		return new AndChain(this.rule);
	}

	public Fee getFee() {
		return fee;
	}

	public void setFee(Fee fee) {
		this.fee = fee;
	}

	@Override
	public String toString() {
		return "RuleDefinition [id=" + id + ", name=" + name + ", mainChain=" + rule + "]";
	}
}
