package fr.cnds.malt.models;

/**
 * Class representing the json sent by a user of our API when he wants to calculate the fees that he should
 * apply.
 */
public class CalculateFeeRequest {
	private Partner client;
	private Partner freelancer;
	private Mission mission;
	private CommercialRelation commercialRelation;
	
	public Partner getClient() {
		return client;
	}
	public void setClient(Partner client) {
		this.client = client;
	}
	public Partner getFreelancer() {
		return freelancer;
	}
	public void setFreelancer(Partner freelancer) {
		this.freelancer = freelancer;
	}
	public Mission getMission() {
		return mission;
	}
	public void setMission(Mission mission) {
		this.mission = mission;
	}
	public CommercialRelation getCommercialRelation() {
		return commercialRelation;
	}
	public void setCommercialRelation(CommercialRelation commercialRelation) {
		this.commercialRelation = commercialRelation;
	}
}
