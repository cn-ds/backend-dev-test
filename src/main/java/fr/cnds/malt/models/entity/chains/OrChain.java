package fr.cnds.malt.models.entity.chains;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import fr.cnds.malt.models.entity.AbstractEntity;

public class OrChain extends AbstractEntityChain {
	
	public OrChain(List<AbstractEntity> unparsedChain) {
		super(unparsedChain);
	}

	@Override
	public boolean evaluate(Map<String, Object> feeRequest) {
		Iterator<AbstractEntity> it = this.chain.iterator();
		boolean oneWasTrue = false;
		while(it.hasNext()) {
			AbstractEntity bloc = it.next();
			oneWasTrue = bloc.evaluate(feeRequest);
			if (oneWasTrue) {
				break;
			}
		}
		return oneWasTrue;
	}

}
