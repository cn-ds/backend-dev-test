package fr.cnds.malt.models.entity.blocs;

import fr.cnds.malt.models.entity.AbstractEntity;

public class GtBloc extends AbstractEntityBloc {

	public GtBloc(AbstractEntity bloc) {
		super(bloc);
	}

	@Override
	public boolean compareInt(Integer actual, Integer expected) {
		return actual > expected;
	}

	@Override
	public boolean compareString(String actual, String expected) {
		return false;
	}

}
