package fr.cnds.malt.models.entity.chains;


import java.util.List;
import java.util.Map;

import fr.cnds.malt.models.entity.AbstractEntity;
import fr.cnds.malt.models.entity.blocs.NoopBloc;

public abstract class AbstractEntityChain extends AbstractEntity {
	
	public AbstractEntityChain(List<AbstractEntity> unparsedChain) {
		if (unparsedChain != null) {
			unparsedChain.forEach(entity -> {
				if (entity.getChain().size() > 0) {
					chain.add(parseChainRule(entity, entity.getChain()));
				} else {
					chain.add(parseBloc(entity));
				}
			});
		} else {
			chain.add(new NoopBloc());
		}
	}
	
	public abstract boolean evaluate(Map<String, Object> feeRequest);	
}
