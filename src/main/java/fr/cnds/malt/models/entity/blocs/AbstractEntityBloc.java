package fr.cnds.malt.models.entity.blocs;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;

import fr.cnds.malt.Constants;
import fr.cnds.malt.models.entity.AbstractEntity;
import fr.cnds.malt.utils.DateUtils;

public abstract class AbstractEntityBloc extends AbstractEntity {
	public AbstractEntityBloc() {
		super();
	}
	
	protected String[] elements;
	
	public AbstractEntityBloc(AbstractEntity bloc) {
		if (bloc.getChain().size() > 0) {
			//TODO handle this case
		} else {
			this.field = bloc.getField();
			elements = this.field.split("\\.");
			this.op = bloc.getField();
			this.value = bloc.getValue();
		}
	}
	public abstract boolean compareInt(Integer actual, Integer expected);
	public abstract boolean compareString(String actual, String expected);
	
	public boolean evaluate(Map<String, Object> feeRequest) {
		int maxSize = elements.length;
		boolean isADuration = false;
		if (this.field.contains(Constants.DURATION)) {
			maxSize -= 1;
			isADuration = true;
		}
		for(int i = 0; i < maxSize; i++) {
			String key = elements[i];
			if (i < maxSize - 1) {
				Map<String, Object> elements = (Map<String, Object>) feeRequest.get(key);
				feeRequest = elements;
			} else {
				if (isADuration) {
					Integer numberFromRule = Integer.valueOf(this.value.replaceAll("\\D+",""));
					Long valueToCompare = (Long) feeRequest.get(elements[maxSize-1]);
					Date dateToCompare = new Date(valueToCompare);
					LocalDate dateFrom = dateToCompare.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
					LocalDate now = LocalDate.now();
					Period period = Period.between(dateFrom, now);
					Integer diff = null;
					if (this.value.contains(Constants.DAYS)) {
						 diff = period.getDays();
					} else if (this.value.contains(Constants.MONTHS)) {
						diff = period.getMonths();
					} else if (this.value.contains(Constants.YEARS)) {
						diff = period.getYears();
					}
					return compareInt(diff, numberFromRule);
					
				} else {
					String valueToCompare = (String) feeRequest.get(key);
					if ( isADuration(this.value) || isADuration(valueToCompare)) {
						return compareInt(convertToDaysQuantity(valueToCompare), convertToDaysQuantity(this.value));
					}
					return compareString(this.value, valueToCompare);
				}
			}
		}
		return false;
	}
	
	public Integer convertToDaysQuantity(String durationString) {
		String key;
		if (durationString.contains(Constants.YEARS)) {
			key = Constants.YEARS;
		} else if (durationString.contains(Constants.MONTHS)) {
			key = Constants.MONTHS;
		} else {
			key = Constants.DAYS;
		}
		return Integer.valueOf(durationString.replaceAll("\\D+","")) * DateUtils.timeScaleConversionRate.get(key);		
	}
	
	/**
	 * Checks that the string is a duration, for example (2months)
	 * @param durationString
	 * @return
	 */
	public boolean isADuration(String durationString) {
		if (durationString == null) {
			return false;
		}
		if (	durationString.contains(Constants.MONTHS) ||
				durationString.contains(Constants.YEARS) ||
				durationString.contains(Constants.DAYS)) {
			return true;
		} else {
			return false;
		}
	}
}
