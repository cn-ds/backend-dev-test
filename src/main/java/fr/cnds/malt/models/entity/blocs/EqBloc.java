package fr.cnds.malt.models.entity.blocs;

import fr.cnds.malt.models.entity.AbstractEntity;

public class EqBloc extends AbstractEntityBloc {

	public EqBloc(AbstractEntity bloc) {
		super(bloc);
	}

	@Override
	public boolean compareInt(Integer actual, Integer expected) {
		return actual.equals(expected);
	}

	@Override
	public boolean compareString(String actual, String expected) {
		return actual.equals(expected);
	}

}
