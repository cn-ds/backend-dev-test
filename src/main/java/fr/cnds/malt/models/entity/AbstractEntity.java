package fr.cnds.malt.models.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fr.cnds.malt.Constants;
import fr.cnds.malt.models.entity.blocs.EqBloc;
import fr.cnds.malt.models.entity.blocs.GtBloc;
import fr.cnds.malt.models.entity.blocs.LtBloc;
import fr.cnds.malt.models.entity.blocs.NoopBloc;
import fr.cnds.malt.models.entity.chains.AbstractEntityChain;
import fr.cnds.malt.models.entity.chains.AndChain;
import fr.cnds.malt.models.entity.chains.OrChain;

public class AbstractEntity {
	protected String op;
	protected String field;
	protected String value;
	protected List<AbstractEntity> chain = new ArrayList<>();
	
	public AbstractEntity() {
		super();
	}
	
	public AbstractEntity parseChainRule(AbstractEntity parent, List<AbstractEntity> unparsedChain) {
		AbstractEntityChain chain;
		switch (parent.getOp()) {
			case Constants.OR:
				chain = new OrChain(unparsedChain);
				break;
			case Constants.AND:
			default:
				chain = new AndChain(unparsedChain);
				break;
		}
		return chain;
	}
	
	public AbstractEntity parseBloc(AbstractEntity unparsedBloc) {
		AbstractEntity bloc;
		switch (unparsedBloc.getOp()) {
			case Constants.GT:
				bloc = new GtBloc(unparsedBloc);
				break;
			case Constants.LT:
				bloc = new LtBloc(unparsedBloc);
				break;
			case Constants.EQ:
				bloc = new EqBloc(unparsedBloc);
				break;
			default:
				bloc = new NoopBloc(unparsedBloc);				
		}
		return bloc;
	}
	
	public boolean evaluate(Map<String, Object> feeRequest) {
		return false;
	}

	public String getOp() {
		return op;
	}

	public void setOp(String op) {
		this.op = op;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public List<AbstractEntity> getChain() {
		return chain;
	}

	public void setChain(List<AbstractEntity> chain) {
		this.chain = chain;
	}
}
