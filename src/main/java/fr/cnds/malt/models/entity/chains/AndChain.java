package fr.cnds.malt.models.entity.chains;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import fr.cnds.malt.models.entity.AbstractEntity;

public class AndChain extends AbstractEntityChain {

	public AndChain(List<AbstractEntity> unparsedChain) {
		super(unparsedChain);
	}

	@Override
	public boolean evaluate(Map<String, Object> feeRequest) {
		return !this.chain.stream()
			.map(bloc -> bloc.evaluate(feeRequest))
			.collect(Collectors.toList())
			.contains(Boolean.FALSE);
	}
}
