package fr.cnds.malt.models.entity.blocs;

import java.util.Map;

import fr.cnds.malt.models.entity.AbstractEntity;

public class NoopBloc extends AbstractEntityBloc {

	public NoopBloc() {
		super();
	}
	
	public NoopBloc(AbstractEntity bloc) {
		super(bloc);
	}

	@Override
	/**
	 * This bloc should always return false it is a fallback bloc for unparsed op
	 */
	public boolean evaluate(Map<String, Object> feeRequest) {
		return false;
	}

	@Override
	public boolean compareInt(Integer actual, Integer expected) {
		return false;
	}

	@Override
	public boolean compareString(String actual, String expected) {
		return false;
	}
}
