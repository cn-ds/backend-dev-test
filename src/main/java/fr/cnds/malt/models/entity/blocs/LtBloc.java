package fr.cnds.malt.models.entity.blocs;

import fr.cnds.malt.models.entity.AbstractEntity;

public class LtBloc extends AbstractEntityBloc {

	public LtBloc(AbstractEntity bloc) {
		super(bloc);
	}

	@Override
	public boolean compareInt(Integer actual, Integer expected) {
		return actual < expected;
	}

	@Override
	public boolean compareString(String actual, String expected) {
		return false;
	}

}
