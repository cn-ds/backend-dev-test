package fr.cnds.malt.models;

import org.springframework.web.client.RestTemplate;

import fr.cnds.malt.Constants;

/**
 * The partner class is used to represent either a client or a freelancer. As they need to have the same
 * attributes it was not needed to build a specific class for each type of partner.
 */
public class Partner {
	private String ip;
	private String country;
	private String city;
	
	public Partner() {
		super();
	}
	
	/**
	 * Using the ip address of the Partner, performs a call to an ip locator web service and retrieves
	 * the country code and the city.
	 */
	private void convertIpToLocation() {
        RestTemplate restTemplate = new RestTemplate();
        IpLocation location = restTemplate.getForObject(Constants.IP_SERVICE_URL + this.ip, IpLocation.class);
        if (Constants.SUCCESS.equals(location.getStatus())) {
    		this.country = location.getCountryCode();
        	this.city = location.getCity();
        } else {
        	this.country = this.city = Constants.NOT_FOUND;
        }
	}
	
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
		convertIpToLocation();
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "Partner [ip=" + ip + ", country=" + country + ", city=" + city + "]";
	}
}
