package fr.cnds.malt.models;

import java.util.Date;

import fr.cnds.malt.utils.DateUtils;

/**
 * Represents the status of the commercial relation of 2 partners.
 */
public class CommercialRelation {	
	private Date firstMission = null;
	private Date lastMission = null;
	
	public void setLastMission(String lastMission) {
		this.lastMission = DateUtils.parseDate(lastMission);
	}
	public void setFirstMission(String firstMission) {
		this.firstMission  = DateUtils.parseDate(firstMission);
	}
	public Date getFirstMission() {
		return firstMission;
	}
	public Date getLastMission() {
		return lastMission;
	}
}
