package fr.cnds.malt;

import java.text.SimpleDateFormat;

public class Constants {
	public final static String IP_SERVICE_URL = "http://ip-api.com/json/";
	public final static String NOT_FOUND = "NOT_FOUND";
	
	/* Error messages for ip-api service */
	public final static String SUCCESS = "success";
	public final static String FAIL = "fail";
	
	/* Arithmetic expressions */
	public final static String EQ = "eq";
	public final static String LT = "lt";
	public final static String GT = "gt";
	public final static String AND = "and";
	public final static String OR = "or";
	
	/* Date handling */
	public final static String DURATION = "duration";
	public final static SimpleDateFormat FORMATER = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS'Z'");
	
	/* Temporal units */
	public final static String MONTHS = "month";
	public final static String YEARS = "year";
	public final static String DAYS = "day";
}
