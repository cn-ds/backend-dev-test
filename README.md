# Backend-dev-test

## Requirements

* mvn
* java 8

## Notes

* Java 8 is the last LTS version I know, I haven't yet taken the time to experiment with Java 11
* I choose spring-boot because it was the opportunity to try it as it is one of the top java framework used
* I updated the json in input
* I misunderstood the duration keyword and the calculation is currently : duration = lastMission - today

## Installation

## Development

````
mvn spring-boot:run
````

## Testing

### Unit tests

````
mvn test
````

### Curl

````
// Add a simple route
curl -X POST http://localhost:8080/rule -H 'Content-Type: application/json' -d '{
    "id": "...",
    "name": "spain or repeat",
    "rule": {}
}'
// Add a complex route
curl -X POST http://localhost:8080/rule -H 'Content-Type: application/json' -d '{
    "id": "route1",
    "name": "spain or repeat",
    "fee" : { "rate":8 },
    "rule": [
        {
            "op":"or",
            "chain": [
                {
                    "op":"gt",
                    "field":"mission.lenght",
                    "value":"2months"
                },
                {
                    "op":"gt",
                    "field":"commercialRelation.lastMission.duration",
                    "value":"2months"
                }
            ]
        },
        {
            "op": "eq",
            "field": "client.country",
            "value": "ES"
        },
        {
            "op": "eq",
            "field": "freelancer.country",
            "value": "ES"
        }
    ]
}'
// Calculate fee
curl -X POST http://localhost:8080/calculateFee -H 'Content-Type: application/json' -d '{
    "client": { "ip":"217.127.206.227" },
    "freelancer": { "ip":"217.127.206.227" },
    "mission": {
        "lenght": "4months"
    },
    "commercialRelation" : {
        "firstMission":"2018-04-16 13:24:17.510Z",
        "lastMission":"2018-07-16 14:24:17.510Z"
    }
}'
// Add a super complex route to test recursivity
curl -X POST http://localhost:8080/rule -H 'Content-Type: application/json' -d '{
    "id": "route1",
    "name": "spain or Madrid",
    "fee" : { "rate":7 },
    "rule": [
        {
            "op":"or",
            "chain": [
                {
                    "op":"gt",
                    "field":"mission.lenght",
                    "value":"2months"
                },
                {
                    "op":"gt",
                    "field":"commercialRelation.lastMission.duration",
                    "value":"6years"
                },
                {
                    "op":"or",
                    "chain": [
                         {
                             "op":"eq",
                             "field":"client.city",
                             "value":"Lyon"
                         },
                         {
                             "op":"eq",
                             "field":"freelancer.city",
                             "value":"Madrid"
                         }
                    ]
                }
            ]
        },
        {
            "op": "eq",
            "field": "client.country",
            "value": "ES"
        },
        {
            "op": "eq",
            "field": "freelancer.country",
            "value": "ES"
        }
    ]
}'
// Calculate fee
curl -X POST http://localhost:8080/calculateFee -H 'Content-Type: application/json' -d '{
    "client": { "ip":"217.127.206.227" },
    "freelancer": { "ip":"217.127.206.227", "city":"Madrid" },
    "mission": {
        "lenght": "1months"
    },
    "commercialRelation" : {
        "firstMission":"2018-04-16 13:24:17.510Z",
        "lastMission":"2018-07-16 14:24:17.510Z"
    }
}'
````